﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using UnityEditor;


namespace com.tamagokobo.UnityUtilities.SyncParticleToAnimation {

    public class AnimationWindowHelper {

        static System.Type animationWindowType;
        static bool m_isOverrideParams = false;
        static GameObject m_overrideRootObject;
        static float m_overrideTime;

        public static float GetAnimationWindowTime() {
            if (m_isOverrideParams)
                return m_overrideTime;

            var animationWindow = GetOpenAnimationWindow();
            if (animationWindow == null)
                return -1f;

            return (float)InvokeAnimationEditorMethod("get_currentTime");
        }

        public static bool IsFocus() {
            if (m_isOverrideParams)
                return true;

            var focusedWindow = EditorWindow.focusedWindow;
            if (focusedWindow == null)
                return false;

            return (focusedWindow.ToString() == " (UnityEditor.AnimationWindow)");
        }

        public static GameObject GetActiveRootGameObject() {
            if (m_isOverrideParams)
                return m_overrideRootObject;

            var animationWindow = GetOpenAnimationWindow();
            if (animationWindow == null)
                return null;

            return (GameObject)InvokeAnimationEditorMethod("get_activeRootGameObject");
        }

        public static List<float> GetParticleResetTimeList(ParticleSystem particle) {
            var animationWindow = GetOpenAnimationWindow();
            if (animationWindow == null)
                return null;

            var timeList = new List<float>();

            var activeAnimationClip = (AnimationClip)InvokeAnimationEditorMethod("get_activeAnimationClip");
            var rootObj = (GameObject)InvokeAnimationEditorMethod("get_activeRootGameObject");

            //アニメデータ解析
            foreach (var binding in AnimationUtility.GetCurveBindings(activeAnimationClip)) {
                if (binding.propertyName != "m_IsActive")
                    continue;

                var bindParticle = rootObj.transform.Find(binding.path).GetComponent<ParticleSystem>();
                if (bindParticle != particle)
                    continue;

                var keys = AnimationUtility.GetEditorCurve(activeAnimationClip, binding).keys;
                foreach (var key in keys) {
                    if (key.value == 1f)
                        timeList.Add(key.time);
                }
                break;
            }

            return timeList;
        }

        public static object InvokeAnimationEditorMethod(string methodName) {
            BindingFlags flags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;

            FieldInfo animEditor = animationWindowType.GetField("m_AnimEditor", flags);
            var animEditorObject = animEditor.GetValue(GetOpenAnimationWindow());

            var state = animEditor.FieldType.GetField("m_State", flags);
            var stateObject = state.GetValue(animEditorObject);

            var method = state.FieldType.GetMethod(methodName, flags);

            return method.Invoke(stateObject, null);
        }

        public static void OverrideParams(GameObject rootObject, float time) {
            m_isOverrideParams = true;
            m_overrideRootObject = rootObject;
            m_overrideTime = time;
        }

        public static void EndOverride() {
            m_isOverrideParams = false;
        }

        static UnityEngine.Object GetOpenAnimationWindow() {
            UnityEngine.Object[] openAnimationWindows = Resources.FindObjectsOfTypeAll(GetAnimationWindowType());
            if (openAnimationWindows.Length > 0) {
                return openAnimationWindows[0];
            }
            return null;
        }

        static System.Type GetAnimationWindowType() {
            if (animationWindowType == null) {
                animationWindowType = System.Type.GetType("UnityEditor.AnimationWindow,UnityEditor");
            }
            return animationWindowType;
        }

    }

}

