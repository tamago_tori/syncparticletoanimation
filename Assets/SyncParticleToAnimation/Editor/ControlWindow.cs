﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace com.tamagokobo.UnityUtilities.SyncParticleToAnimation {
    public class ControlWindow : EditorWindow {

        static ControlWindow m_window;

        static float m_currentTime = 0f;
        static int m_currentFrame = 0;
        static float m_lastEditorTime = 0f;
        static int m_maxFrame = 600;
        static int m_sample = 60;
        static bool m_isPlay = false;

        [MenuItem("Tools/SyncParticleToAnimation/OpenControlWindow")]
        public static void OpenWindow() {
            if (m_window != null)
                return;

            m_window = CreateInstance<ControlWindow>();
            m_window.titleContent.text = "SyncParticle";
            m_window.minSize = new Vector2(400f, 140f);
            m_window.ShowUtility();
        }

        void OnGUI() {
            //=========
            //particle only timeline
            //=========

            //setting
            EditorTimelineSetting();

            //timeline
            EditorParticleTimeline();

            //line
            GUILayout.Space(10);
            GUILayout.Box("", GUILayout.ExpandWidth(true), GUILayout.Height(1));
            GUILayout.Space(10);

            //============
            //sync
            //============
            var isSync = SyncParticleToAnimation.IsSync;

            if (isSync) {
                GUI.backgroundColor = Color.red;
            } else {
                GUI.backgroundColor = Color.white;
            }

            if (GUILayout.Button(isSync ? "Stop Sync" : "Start Sync")) {
                isSync = SyncParticleToAnimation.SwitchSync();
            }
        }

        void EditorTimelineSetting() {
            EditorGUILayout.LabelField("particle timeline");
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button(m_isPlay ? "Stop" : "Play")) {
                SwitchPlay();
            }
            if (GUILayout.Button("<<")) {
                PrevFrame();
            }
            if (GUILayout.Button(">>")) {
                NextFrame();
            }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            m_maxFrame = EditorGUILayout.IntField("MaxFrame", m_maxFrame);
            m_sample = EditorGUILayout.IntField("Sample", m_sample);
            EditorGUILayout.EndHorizontal();
        }

        void EditorParticleTimeline() {
            var frame = EditorGUILayout.IntSlider(m_currentFrame, 0, m_maxFrame);

            if (m_currentFrame == frame && !m_isPlay)
                return;

            m_currentFrame = frame;
            UpdateParticle();
        }

        static void UpdateParticle() {
            m_currentTime = m_currentFrame * (1f / m_sample);
            SyncParticleToAnimation.UpdateParticleTimeline(m_currentTime);
        }

        static void UpdateTimeline() {
            if(m_lastEditorTime == 0f)
                m_lastEditorTime = (float)EditorApplication.timeSinceStartup;

            m_currentTime += (float)EditorApplication.timeSinceStartup - m_lastEditorTime;
            m_lastEditorTime = (float)EditorApplication.timeSinceStartup;

            SyncParticleToAnimation.UpdateParticleTimeline(m_currentTime);

            m_currentFrame = (int)(m_currentTime / (1f / m_sample));
            if(m_currentFrame > m_maxFrame) {
                SwitchPlay();
            }
        }

        static void SwitchPlay() {
            m_isPlay = (!m_isPlay);
            if (m_isPlay) {
                EditorApplication.update += UpdateTimeline;
            } else {
                m_lastEditorTime = 0f;
                EditorApplication.update -= UpdateTimeline;
            }
        }

        static void NextFrame() {
            if (++m_currentFrame > m_maxFrame)
                m_currentTime = m_maxFrame;

            UpdateParticle();
        }

        static void PrevFrame() {
            if (--m_currentFrame < 0)
                m_currentFrame = 0;

            UpdateParticle();
        }
    }
}

