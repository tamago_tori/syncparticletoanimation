﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace com.tamagokobo.UnityUtilities.SyncParticleToAnimation {

    [ExecuteInEditMode]
    public class SyncParticleToAnimation : MonoBehaviour {

        static List<ParticleSystem> m_particleSystemList = new List<ParticleSystem>();
        static Dictionary<ParticleSystem, List<float>> m_particleResetTimeListDict = new Dictionary<ParticleSystem, List<float>>();
        static float m_currentTime;
        static float m_prevTime;
        static float m_deltaTime;
        static bool m_isRestart;
        static bool m_isFocusAnimationWindow = false;
        static bool m_isPrevFocusAnimationWindow = false;
        static bool m_isSync = false;
        public static bool IsSync {
            get { return m_isSync; }
        }
        static Transform m_activeRootTransform;
        static Transform m_prevRootTransform;

        // Update is called once per frame
        [MenuItem("Tools/SyncParticleToAnimation/StartSync")]
        public static void StartSync() {
            if (m_isSync)
                return;

            m_isSync = true;
            EditorApplication.update += UpdateFunc;
        }

        [MenuItem("Tools/SyncParticleToAnimation/StopSync")]
        public static void StopSync() {
            if (!m_isSync)
                return;

            m_isSync = false;
            EditorApplication.update -= UpdateFunc;
        }

        public static bool SwitchSync() {
            if (m_isSync) {
                StopSync();
            } else {
                StartSync();
            }

            return m_isSync;
        }

        public static void UpdateParticleTimeline(float time) {
            if (m_isSync)
                return;

            var rootObject = GetRootParticleObject(Selection.activeGameObject);
            if (rootObject == null)
                return;

            AnimationWindowHelper.OverrideParams(rootObject, time);
            UpdateFunc();
            AnimationWindowHelper.EndOverride();
        }

        static void UpdateFunc() {
            if (EditorApplication.isPlaying)
                return;

            m_isFocusAnimationWindow = AnimationWindowHelper.IsFocus();

            //clear inactive particles
            UpdateActiveParticleSystems();
            if (m_activeRootTransform != m_prevRootTransform) {
                m_prevRootTransform = m_activeRootTransform;
                ClearInactiveParticles();
            }

            //check focus window
            if (!m_isFocusAnimationWindow) {
                m_isPrevFocusAnimationWindow = false;
                return;
            }

            //get time
            m_currentTime = AnimationWindowHelper.GetAnimationWindowTime();
            if (m_currentTime == -1f)
                return;

            m_deltaTime = m_currentTime - m_prevTime;

            //check update
            m_isRestart = CheckRestart(); 
            if (m_deltaTime == 0f && !m_isRestart)
                return;

            //update particle
            UpdateParticles();

            //post
            m_prevTime = m_currentTime;
            m_isPrevFocusAnimationWindow = true;
            m_prevRootTransform = m_activeRootTransform;
        }

        static void UpdateActiveParticleSystems() {
            var activeGameObject = AnimationWindowHelper.GetActiveRootGameObject();
            if(activeGameObject == null) {
                m_particleSystemList.Clear();
                m_particleResetTimeListDict.Clear();
                return;
            }

            //update
            m_activeRootTransform = activeGameObject.transform;
            var particleArray = m_activeRootTransform.GetComponentsInChildren<ParticleSystem>(true);
            m_particleSystemList = new List<ParticleSystem>(particleArray);
        }

        static void UpdateParticles() {

            if (m_isRestart) {
                //Active切り替えタイム取得
                foreach (var particle in m_particleSystemList) {
                    m_particleResetTimeListDict[particle] = AnimationWindowHelper.GetParticleResetTimeList(particle);
                }
                //親のタイムを継承
                foreach (var particle in m_particleSystemList) {
                    InheritResetTimeList(particle, particle.transform);
                    m_particleResetTimeListDict[particle].Sort();
                }
            }

            foreach(var particle in m_particleSystemList){
                var deltaTime = m_deltaTime;

                //restart
                if (m_isRestart) {
                    particle.Simulate(0f, true, true);
                    deltaTime = m_currentTime;
                }

                //check particle reset
                var isReset = false;
                var resetTimeList = m_particleResetTimeListDict[particle];
                while (resetTimeList.Count > 0 && m_currentTime > resetTimeList[0]) {
                    isReset = true;
                    deltaTime = m_currentTime - resetTimeList[0];
                    resetTimeList.RemoveAt(0);
                }
                if (isReset) {
                    particle.Simulate(0f, true, true);
                }

                //play
                particle.Simulate(deltaTime, false, false);
            };

            m_isRestart = false;
        }

        static void InheritResetTimeList(ParticleSystem particle, Transform particleTransform) {
            var parentTransform = particleTransform.parent;
            if (parentTransform == null)
                return;
            var parentParticle = parentTransform.GetComponent<ParticleSystem>();
            if (parentParticle == null)
                return;

            m_particleResetTimeListDict[particle].AddRange(m_particleResetTimeListDict[parentParticle]);
            InheritResetTimeList(particle, parentTransform);
        }

        static bool CheckRestart() {
            if (m_deltaTime < 0f)
                return true;
            if (AnimationWindowHelper.IsFocus() && !m_isPrevFocusAnimationWindow)
                return true;

            return false;
        }

        static void ClearInactiveParticles() {
            var allParticles = new List<ParticleSystem>(FindObjectsOfType<ParticleSystem>());
            allParticles.ForEach((particle) => {
                var isActive = m_particleSystemList.Find((activeParticle) => {
                    return (particle == activeParticle);
                });

                if (isActive)
                    return;

                particle.Clear();
            });
        }

        static GameObject GetRootParticleObject(GameObject selection) {
            if (selection == null)
                return null;

            ParticleSystem currentParticle = selection.transform.GetComponent<ParticleSystem>();
            if (currentParticle == null)
                return null;
            if (selection.transform.parent == null)
                return selection;

            ParticleSystem parentParticle = currentParticle.transform.parent.GetComponent<ParticleSystem>();
            while(parentParticle != null) {
                currentParticle = parentParticle;
                if (currentParticle.transform.parent == null)
                    return currentParticle.gameObject;

                parentParticle = currentParticle.transform.parent.GetComponentInParent<ParticleSystem>();
            }

            return currentParticle.gameObject;
        }

    }
}
